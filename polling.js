
const MAX_ATTEMPT = 5; 

let initPoll = false, 
    pollId, 
    roomName = '', 
    participantId = '', 
    pollUrl='vcAdapter/getSocketMessages',
    postMessageUrl='vcAdapter/postSocketMessages',
    pollInterval = 5000,
    thresholdTime =0 ;

let messageCallbackArr = [] ;

const getSocketMessage = (json) => {
    if (json) {
      return {
        target: json.target,
        type: json.type,
        state: json.payload ? json.payload.state : {}
      };
    } else {
      return {};
    }
  };

export const initializePoll = ({uid, roomId, msgCallbackArr, source, apiBaseUrl, interval}) => {
    if(initPoll)
        return ;

    roomName = roomId ;
    participantId = uid ;
    pollUrl = `${apiBaseUrl}${pollUrl}`
    messageCallbackArr = msgCallbackArr ;
    initPoll = true; 
    pollInterval = interval || pollInterval; 
    thresholdTime = new Date().getTime() ;
    pollId = setInterval(master,pollInterval, source) ;
}

export const stopPoll = () => {
    if(!initPoll)
        return ;
    
    clearInterval(pollId) ;
    initPoll = false ;
}

const master = (source) =>{
    
    let lastTime = thresholdTime ;
    thresholdTime += pollInterval ; 
    getSocketMessages(source, lastTime, thresholdTime) ;
}

const getSocketMessages = (source, lastTime, currTime) => {

    fetch(`${pollUrl}?roomId=${roomName}&uid=${participantId}&lastTime=${lastTime}&currTime=${currTime}&source=${source}`)
    .then(response => response.json())
    .then(res => {
        console.log("Poll Messages : ", res.data) ;
        processMessages(res.data) ;
    })
    .catch(err => console.log("ERROR : ", err));
}
 
const processMessages = (messagesData) => {
    const {messageList , commonMessages} = messagesData
    messageIdx += messageList.length;
    commonMessageIdx += commonMessages.length ;
    
    let messages = messageList.concat(commonMessages) ;
    if(!messages.length) 
        return ;

    messages.sort(function(m1, m2) {return m1.sentAt - m2.sentAt;})

    messages.forEach(message => {
        messageCallbackArr.forEach(callback => {
            let msg = getSocketMessage(message)
            callback(msg)
        });
    });
}

export const postSocketMessages = (data, apiBaseUrl) => {

    fetch(`${apiBaseUrl}${postMessageUrl}`, {
        method : 'POST',
        headers : {
            "Content-type": "application/json; charset=UTF-8"
        },
        mode : 'cors',
        body : JSON.stringify(data) 
    })
    .then(res => console.log(res))
    .catch(err => console.log(err)) ; 
}