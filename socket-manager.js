import { socketInstance } from "./socket-instance.js"

let connections = {};

export const socketManager = ({ uid, roomId, socketToken, source, socketUrl, blocked, max_attempts, apiBaseUrl, interval}) => {
  const key = `${uid}-${roomId}`;
  if (!connections[key] || connections[key].closed) {
    connections[key] = socketInstance({ uid, roomId, url: socketUrl, source, blocked, max_attempts, apiBaseUrl, interval});
    connections[key].init(socketToken);
  }
  return connections[key];
};
