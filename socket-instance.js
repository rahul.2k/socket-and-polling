import Sockette from "./sockette"
import { initializePoll, postSocketMessages, stopPoll } from "./polling.js";

var sequenceNum = 1 ;

const PING_INTERVAL = 50000;

const isJSON = function(s) {
  try {
    return JSON.parse(s);
  } catch (e) {
    return false;
  }
};

const getSocketData = ({
  uid,
  roomId,
  target = "*",
  type = "COMMAND",
  state = {},
  action = "GLOBAL_STATE",
  updateState = false
}) => ({
  action: "sendMessage",
  body: {
    version: "1.0",
    type,
    payload: {
      action: action,
      updateState,
      state
    },
    priority: 3,
    senderDetails: {
      uid,
      roomId
    },
    target,
    relatedMessageId: "",
    sentAt: new Date().toISOString(),
    forwardedAt: null,
    sequenceNum : sequenceNum++
  }
});

const getSocketMessage = ({ data }) => {
  const json = isJSON(data);
  if (json && json.body) {
    return {
      target: json.body.target,
      type: json.body.type,
      state: json.body.payload ? json.body.payload.state : {}
    };
  } else {
    return data;
  }
};


const SOCKET_EVENTS = {
  CONNECT:"CONNECT",
  CLOSE:"CLOSE",
  MESSAGE:"MESSAGE",
  RECONNECT:"RECONNECT",
  MAXIMUM:"MAXIMUM",
  ERROR:"ERROR",
}

export const socketInstance = function({ uid, roomId, url, source, blocked , max_attempts, apiBaseUrl, interval}) {

  let events = {};

  const on = (k,f)=>{
    if (!events[k]) events[k] = [];
    events[k].push(f);
  };

  const off = (k,f)=>{
    events[k] = (events[k]||[]).filter((v)=>v!==f);
  };

  const trigger = (k,v)=>{
    (events[k]||[]).forEach((f)=>{
      if (typeof f === 'function') f(v);
    });
  };

  let initPoll = false ;

  let ws = {
    closed: false,
    connected: false,
    onConnect: fn => on(SOCKET_EVENTS.CONNECT, fn),
    offConnect: fn => off(SOCKET_EVENTS.CONNECT, fn),
    onMessage: fn => on(SOCKET_EVENTS.MESSAGE, fn),
    offMessage: fn => off(SOCKET_EVENTS.MESSAGE, fn),
    onReconnect: fn => on(SOCKET_EVENTS.RECONNECT, fn),
    offReconnect: fn => off(SOCKET_EVENTS.RECONNECT, fn),
    onMaximum: fn => on(SOCKET_EVENTS.MAXIMUM, fn),
    offMaximum: fn => off(SOCKET_EVENTS.MAXIMUM, fn),
    onClose: fn => on(SOCKET_EVENTS.CLOSE, fn),
    offClose: fn => off(SOCKET_EVENTS.CLOSE, fn),
    onError: fn => on(SOCKET_EVENTS.ERROR, fn),
    offError: fn => off(SOCKET_EVENTS.ERROR, fn),
    init: function(token) {
      let pollId = null;

      const socket = Sockette(`${url}?token=${token}`, {
        timeout: 5e3,
        maxAttempts: max_attempts,
        onopen: e => {
          pollId = setInterval(() => {
            socket.json({
              action: "sendMessage",
              body: { data: "PING" }
            });
          }, PING_INTERVAL);
          ws.connected = true;
          ws.closed = false ;
          if(initPoll) {
            initPoll = false ;
            stopPoll() ;
          }
          if(blocked) socket.breakSocket(3002, "Simulation Error")
          trigger(SOCKET_EVENTS.CONNECT,e);
        },
        onmessage: e => {
          const msg = getSocketMessage(e);
          trigger(SOCKET_EVENTS.MESSAGE,msg);
        },
        onreconnect: e => {
          trigger(SOCKET_EVENTS.RECONNECT,e);
        },
        onmaximum: e => {
          trigger(SOCKET_EVENTS.MAXIMUM,e);
          initPoll = true;
          initializePoll({uid, roomId, msgCallbackArr : events[SOCKET_EVENTS.MESSAGE], source, apiBaseUrl, interval});
        },
        onclose: e => {
          trigger(SOCKET_EVENTS.CLOSE,e);
        },
        onerror: e => {
          trigger(SOCKET_EVENTS.ERROR,e);
        }
      });

      ws.send = function(options) {
        const json = getSocketData({ uid, roomId, ...options });
        try {
          let msg = json.body
          postSocketMessages({roomId, message : msg, source, blocked}, apiBaseUrl) 
        } catch (err) {
          console.log(err) 
        }

        socket.json(json);
      };

      ws.requestState = function() {
        const json = getSocketData({ uid, roomId, type: "REQUEST_STATE" });
        socket.json(json);
      };

      ws.updateState = function(state) {
        const json = getSocketData({
          uid,
          roomId,
          type: "COMMAND",
          updateState: true,
          state
        });

        socket.json(json);
      };

      ws.close = function() {
        socket.close();
        events = {};
        ws.closed = true;
        ws.connected = false;
      };
      /**
       * Don't use methods mentioned below this line. It's only for legacy code support
       */
      ws.sendMessage = (payload) => {
        try {
          if (!socket) {
            return;
          }
          socket.json({
            action: 'sendMessage',
            body: payload
          });
        } catch (e) {
          console.log(e, 'e>>>');
        }
      }

      delete ws.init;
    },
    /**
     * Don't use methods mentioned below this line. It's only for legacy code support
     */
    onEvent: (identifier, callback) => on(identifier, callback),
  };

  return ws;
};
