import { socketManager } from "./socket-manager.js";

let socketInstance = null;

export const initSocketService = ({
  uid, 
  roomId,
  socketToken,
  cb,
  connectCb,
  source,
  socketUrl,
  blocked,
  max_attempts,
  apiBaseUrl,
  interval
}) => {
  socketInstance = socketManager({
    uid,
    roomId,
    source,
    socketUrl,
    blocked,
    max_attempts,
    apiBaseUrl,
    interval,
    socketToken:
      socketToken ||
      btoa(
        JSON.stringify({
          uid: uid,
          roomId: roomId
        })
      )
  });

  if (socketInstance && socketInstance.connected && connectCb) {
    connectCb();
  } else {
    socketInstance.onConnect(connectCb);
    socketInstance.onMessage(cb);
  }
};

export const closeSocketService = () => {
  if (socketInstance) socketInstance.close();
};

export const sendSocketMessage = msg => {
  socketInstance && socketInstance.connected && socketInstance.send(msg);
};

export const getSocketInstance = () => socketInstance

