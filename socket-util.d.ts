export declare function initSocketService(params : initObjType): any;

export declare function closeSocketService(): any;

export declare function sendSocketMessage(msg : object): any;

export declare function getSocketInstance(): object;

interface initObjType {
    uid: string, 
    roomId : string, 
    socketToken: string, 
    socketUrl : string,
    source : string,
    cb: function , 
    connectCb: function,
    blocked: boolean,
    max_attempts: number,
    apiBaseUrl:string
}